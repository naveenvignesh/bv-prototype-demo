# NativeScript Prototype Application

This app is was created to test the performance of the nativescript framework.

It implements the pro ui for nativescript provided by telerik.

You can add it to package by using the following command.

```
npm i nativescript-pro-ui
```
Basic template is created using the following command

```
tns create my-app-name --ng
```

> Note: The above command will create a template with a single component as base template

If you want to add a component install nativescript-cli using the command

```
npm install -g nativescript-cli
```

Following commands are used to add components, services, shared modules to app to the app

Command to add a component
```
tns generate component component-name

tns g c component-name
```

This project is synced to Brainvalley Slack Workplace.

Prototype done by: Naveen Vignesh.B 

**NB:** Please, have in mind that all cli commands must be execute within the project directory
