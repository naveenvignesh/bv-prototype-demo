import { Injectable } from '@angular/core';
var Sqlite = require("nativescript-sqlite");

@Injectable()
export class StorageService {
  private database: any;
 
  constructor() {
    (new Sqlite("data.db")).then(db => {
      this.database = db;
    },err=>console.log(err));
   }

  insert(value) {
    return this.database.execSQL("INSERT INTO sample values(?)",[value]);
  }

  fetch() {
    return this.database.all("SELECT name FROM sample");
  }

  delete(value) {
    return this.database.execSQL("DELETE FROM sample WHERE name = ?",[value]);
  }

  createTables() {
    // this.db.execSQL("create table if not exists sample (name text)");
    // this.database.execSQL("create table if not exists image (img blob)");
    (new Sqlite("data.db")).then(db => {
      db.execSQL("create table if not exists sample (name text)");
      db.execSQL("create table if not exists image (sno number primary key,img text)");

    });
  }

  saveImageToDB(data) {
    this.database.execSQL("INSERT OR IGNORE INTO image values (?,?)", [1, ""]);
    return this.database.execSQL("UPDATE image SET img = ? WHERE sno = 1",[data]);
  }

  loadImageFromDB() {
    return this.database.all("SELECT img from image where sno = 1");
  }

}