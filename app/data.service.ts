import { Injectable } from '@angular/core';

export class Country {
  constructor(Country:string,Amount:number,SecondVal:number,ThirdVal:number,Impact:number,Year:number) {}
}

export class Car {
  constructor(Brand: string, Amount: number) { }
}

@Injectable()
export class DataService {

  constructor() { }

  getCategoricalSource(): Country[] {
    return [
      { Country: "Germany", Amount: 20, SecondVal: 17, ThirdVal: 20, Impact: 0, Year: 0 },
      { Country: "France", Amount: 14, SecondVal: 22, ThirdVal: 25, Impact: 0, Year: 0 },
      { Country: "Bulgaria", Amount: 20, SecondVal: 20, ThirdVal: 23, Impact: 0, Year: 0 },
      { Country: "Spain", Amount: 11, SecondVal: 19, ThirdVal: 24, Impact: 0, Year: 0 },
      { Country: "USA", Amount: 11, SecondVal: 18, ThirdVal: 15, Impact: 0, Year: 0 }
    ];
  }

  getPieSource(): Car[] {
    return [
      { Brand: "Audi", Amount: 10 },
      { Brand: "Mercedes", Amount: 76 },
      { Brand: "Fiat", Amount: 60 },
      { Brand: "BMW", Amount: 24 },
      { Brand: "Crysler", Amount: 40 }
    ];
  }
}
