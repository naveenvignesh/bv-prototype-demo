import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-datalist',
  templateUrl: './datalist.component.html',
  styleUrls: ['./datalist.component.scss']
})
export class DatalistComponent implements OnInit {

  item:any;

  constructor(private route:ActivatedRoute) {
    this.route.queryParams.subscribe(data=>{
      this.item = data;
    })
  }

  ngOnInit() { }

}
