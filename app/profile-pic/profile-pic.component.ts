import { Component, OnInit } from '@angular/core';
import { StorageService } from "./../storage.service";
import * as imagepicker from "nativescript-imagepicker";
import { ImageSource } from 'tns-core-modules/image-source/image-source';

@Component({
  moduleId: module.id,
  selector: 'app-profile-pic',
  templateUrl: './profile-pic.component.html',
  styleUrls: ['./profile-pic.component.scss']
})
export class ProfilePicComponent implements OnInit {
  
  picSrc:any;
  context:any;
  picData:any;

  constructor(private storage:StorageService) { 
    // initializing image picking context to pick for single image
    this.context = imagepicker.create({
      mode: "single"
    });
  }

  ngOnInit() {
    this.loadImage();
  }

  pickImage() {
    this.context.authorize()
    .then(()=>{
      return this.context.present();
    })
    .then((selection)=>{
      // do something with selected image
      this.picSrc = selection[0].fileUri;
      console.log(this.picSrc);
      selection[0].getImage().then(res => {
        
        this.picData = "";
        this.picData = res.toBase64String("png");
        // console.log(this.picData);
      })
    }).catch(err=>console.log(err));
  }

  saveImage() {
    if(this.picData) {
      this.storage.saveImageToDB(this.picData).then(() => {
        console.log("Image saved");
      },err=>console.log(err));
    }
  }

  loadImage() {
    let imgsrc = new ImageSource();
    this.storage.loadImageFromDB().then((rows)=>{
      var loadedBase64Img = imgsrc.loadFromBase64(rows[0][0]);
      if(loadedBase64Img) {
        this.picSrc = imgsrc;
      }
    });
  }
}
