import { Component, OnInit } from '@angular/core';
import { TextField } from "ui/text-field";
import { Page } from "ui/page";
import { Router } from "@angular/router";
import * as Toast from 'nativescript-toast';

@Component({
  moduleId: module.id,
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  // ui components
  private tf1: TextField;
  private tf2: TextField;
  private tf3: TextField;
  private tf4: TextField;

  constructor(private page: Page, private router: Router) { }

  ngOnInit() {
    
    // initialising components
    this.tf1 = <TextField>this.page.getViewById("nametf");
    this.tf2 = <TextField>this.page.getViewById("phonetf");
    this.tf3 = <TextField>this.page.getViewById("mailtf");
    this.tf4 = <TextField>this.page.getViewById("passtf");

  }

  gotoListPage() {
    let navExtras = {
      queryParams: {
        "name":this.tf1.text,
        "phone":this.tf2.text,
        "mail":this.tf3.text,
        "pass":this.tf4.text
      }
    }

    if (this.tf1.text && this.tf2.text && this.tf3.text && this.tf4.text) {
      this.router.navigate(['datadisplay'],navExtras);
    }
    else this.toast("Enter all the fields");
  }

  toast(msg) {
    Toast.makeText(msg).show();
  }
}
