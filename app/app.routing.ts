import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { GaugeComponent } from "./gauge/gauge.component";
import { FormComponent } from "./form/form.component";
import { DatalistComponent } from "./datalist/datalist.component";
import { ListComponent } from "./list/list.component";
import { DrawerComponent } from "./drawer/drawer.component";
import { ChartComponent } from "./chart/chart.component";
import { EntryComponent } from "./entry/entry.component";
import { DynamicChartComponent } from "./dynamic-chart/dynamic-chart.component";
import { ProfilePicComponent } from "./profile-pic/profile-pic.component";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: HomeComponent },
    { path: "gauge", component: GaugeComponent },
    { path: "form", component: FormComponent },
    { path: "datadisplay", component: DatalistComponent },
    { path: "list",component:ListComponent},
    { path: "drawer",component:DrawerComponent},
    { path: "chart",component:ChartComponent},
    { path: "entry",component:EntryComponent},
    { path: "dynamic_chart",component:DynamicChartComponent},
    { path: "profile",component:ProfilePicComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}