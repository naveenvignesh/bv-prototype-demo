import { Component, OnInit } from '@angular/core';
import { TextField } from "ui/text-field";
import { Page } from "ui/page";
import { getViewById } from 'tns-core-modules/ui/editable-text-base/editable-text-base';

import * as Toast from "nativescript-toast";

@Component({
  moduleId: module.id,
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public dataItems = [];
  private nameField: TextField;
  private phoneField: TextField;

  constructor(private page:Page) { }

  ngOnInit() {
    //initializing components
    this.nameField = <TextField>this.page.getViewById("tf1");
    this.phoneField = <TextField>this.page.getViewById("tf2");
   }

  addToList() {

    if (this.nameField.text && this.phoneField.text) {

      var obj = {
        name: this.nameField.text,
        phone: this.phoneField.text,
        vote: 0
      }

      this.dataItems.push(obj);

    }
    else this.toast("Enter all fields");
  }

  private toast(msg) {
    Toast.makeText(msg).show();
  }

  public upvote(obj) {
    obj.vote++;
    this.sortArray();
  }

  public downvote(obj) {
    if (obj.vote > 0) obj.vote--;
    this.sortArray();
  }

  private sortArray() {
    this.dataItems.sort((a, b) => {
      if (a.vote > b.vote) return -1;
      else if (a.vote < b.vote) return 1;
      else return 0;
    });
  }
}
