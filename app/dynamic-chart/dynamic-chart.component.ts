import { Component, OnInit } from '@angular/core';
import { ObservableArray } from "data/observable-array";
import { Page } from "ui/page";
import { DataService,Car } from './../data.service';
import { TextField } from "ui/text-field";
import { Button } from "ui/button";
import { android } from 'tns-core-modules/application/application';
import { NUMBER_TYPE } from '@angular/compiler/src/output/output_ast';

@Component({
  moduleId: module.id,
  selector: 'app-dynamic-chart',
  templateUrl: './dynamic-chart.component.html',
  styleUrls: ['./dynamic-chart.component.scss']
})

export class DynamicChartComponent implements OnInit {

  private _pieSource: ObservableArray<Car>;

  protected auditf:TextField;
  protected mercedestf: TextField;
  protected faittf: TextField;
  protected bmwtf: TextField;
  protected cryslertf: TextField;
  protected bt:Button;

  constructor(private _dataService: DataService,private page:Page) { }

  get pieSource(): ObservableArray<Car> {
    return this._pieSource;
  }

  ngOnInit() {
    this._pieSource = new ObservableArray(this._dataService.getPieSource());

    this.gui();
  }

  gui() {
    //initializing gui components
    this.auditf = <TextField>this.page.getViewById("auditf");
    this.mercedestf = <TextField>this.page.getViewById("mercedestf");
    this.faittf = <TextField>this.page.getViewById("faittf");
    this.bmwtf = <TextField>this.page.getViewById("bmwtf");
    this.cryslertf = <TextField>this.page.getViewById("cryslertf");
    
    this.bt = <Button>this.page.getViewById("submit");
  }

  focusTextField(tf:TextField) {
    tf.focus();
  }

  focusButton() {
    this.bt.focus();
  }
  changeChart() {
    if (this.auditf.text && this.bmwtf.text && this.cryslertf.text && this.faittf.text && this.mercedestf.text) {
      var array = new ObservableArray([
        { Brand: "Audi", Amount: +this.auditf.text },
        { Brand: "Mercedes", Amount: +this.bmwtf.text },
        { Brand: "Fiat", Amount: +this.cryslertf.text },
        { Brand: "BMW", Amount: +this.faittf.text },
        { Brand: "Crysler", Amount: +this.mercedestf.text }
      ]);

      this._pieSource = array;
    }
  }
}
