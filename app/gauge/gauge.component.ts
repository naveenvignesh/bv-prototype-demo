import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgModule } from '@angular/core/src/metadata/ng_module';
import { RadialNeedle } from "nativescript-pro-ui/gauges";
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  moduleId: module.id,
  selector: 'app-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.scss']
})

export class GaugeComponent implements AfterViewInit {

  private _needle: RadialNeedle;
  private counter: number = 0;

  @ViewChild("needle") needleElement: ElementRef;

  constructor() { 
   
  }

  ngAfterViewInit() {
    this._needle = this.needleElement.nativeElement as RadialNeedle;
  }

  public incrementGauge() {
    this.counter++;
    this._needle.value = this.counter;
    if (this.counter == 6) this.counter = -1;
  }  

}