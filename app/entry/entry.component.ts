import { Component, OnInit } from '@angular/core';
import { StorageService } from "./../storage.service";
import { Page } from "ui/page";
import { TextField } from "ui/text-field";
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  moduleId: module.id,
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss']
})
export class EntryComponent implements OnInit,AfterViewInit {
  private tf:TextField;
  public dataItems = [];
  constructor(private storage:StorageService,private page:Page) {
    
  }

  ngOnInit() {
    this.tf = <TextField>this.page.getViewById("tf");
  }

  ngAfterViewInit() {
    this.loadSQLData();
  }

  loadSQLData() {
    this.dataItems = []; // emptying array
    this.storage.fetch().then(rows => { //fetching rows
      for (var row of rows) { //iterating row one by one
        this.dataItems.push({ name: row[0] }); // push json object to array
      }
    });
  }

  addToDB() {
    if(this.tf.text) {
      this.storage.insert(this.tf.text).then(()=>{
        console.log("Inserted to SQLite DB");
        this.loadSQLData(); // updating list by binding data from sql
      },err=>console.log(err));
    }
  }

  deleteFromDB(obj) {
    if(obj.name) {
      this.storage.delete(obj.name).then(()=>{
        this.loadSQLData(); //updating list by binding data from sql
      });
    }
  }
}
