import { Component } from "@angular/core";
import { StorageService } from "./storage.service";


@Component({
    selector: "ns-app",
    templateUrl: "app.component.html",
})

export class AppComponent {
    private database: any;

    constructor(private storage:StorageService) {
      this.storage.createTables(); //service method to create tables for sqlite
    }
}
