import { Component, OnInit } from '@angular/core';
import { Country,DataService } from "./../data.service";
import { ObservableArray } from "tns-core-modules/data/observable-array";

@Component({
  moduleId: module.id,
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  private _categoricalSource: ObservableArray<Country>;

  constructor(private _dataService: DataService) { }

  get categoricalSource(): ObservableArray<Country> {
    return this._categoricalSource;
  }

  ngOnInit() {
    this._categoricalSource = new ObservableArray(this._dataService.getCategoricalSource());
  } 

}
