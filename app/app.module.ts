import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";

import { HomeComponent } from "./home/home.component";
import { GaugeComponent } from "./gauge/gauge.component";
import { FormComponent } from "./form/form.component";
import { DatalistComponent } from "./datalist/datalist.component";
import { ListComponent } from "./list/list.component";
import { DrawerComponent } from "./drawer/drawer.component";
import { ChartComponent } from "./chart/chart.component";
import { EntryComponent } from "./entry/entry.component";
import { DynamicChartComponent } from "./dynamic-chart/dynamic-chart.component";
import { ProfilePicComponent } from "./profile-pic/profile-pic.component";

import { NativeScriptUIGaugesModule } from "nativescript-pro-ui/gauges/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-pro-ui/sidedrawer/angular";
import { NativeScriptUIChartModule } from "nativescript-pro-ui/chart/angular";

import { StorageService } from "./storage.service";
import { DataService } from "./data.service";
// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpModule } from "nativescript-angular/http";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptUIGaugesModule,
        NativeScriptUISideDrawerModule,
        NativeScriptUIChartModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        GaugeComponent,
        FormComponent,
        DatalistComponent,
        ListComponent,
        DrawerComponent,
        ChartComponent,
        EntryComponent,
        DynamicChartComponent,
        ProfilePicComponent
    ],
    providers: [
        StorageService,DataService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class AppModule { }
